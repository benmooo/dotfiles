# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from typing import List  # noqa: F401

from libqtile import qtile
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord
from libqtile.lazy import lazy
# from libqtile.utils import guess_terminal

mod = "mod4"
# terminal = guess_terminal()
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),


    # Customize by aka
    # Key([mod], "p", lazy.spawn("dmenu_run -p 'Run: '"), desc='Run Launcher'),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc='toggle floating'),
    Key([mod], "m", lazy.layout.maximize(),
        desc='toggle window (minimum & maximum)'),
    Key([mod], "b", lazy.screen.togglegroup(), desc='toggle group'),
    # Key chord bindings for programs
    KeyChord([mod], "p", [
        Key([], "c", lazy.spawn("codium"), desc="vscode"),
        Key([], "n", lazy.spawn("nitrogen"), desc="start nitrogen"),
        Key([], "l", lazy.spawn("slock"), desc="lock screen"),
    ]),
    # Key chord bindings for browser -> edge
    KeyChord([mod], "e", [
        Key([], "p", lazy.spawn("microsoft-edge-dev --inprivate"),
            desc="open browser in private mode"),
        Key([], "g", lazy.spawn("microsoft-edge-dev https://github.com"), desc="github"),
        Key([], "b", lazy.spawn(
            "microsoft-edge-dev https://bilibili.com"), desc="bilibili"),
        Key([], "t", lazy.spawn(
            "microsoft-edge-dev https://translate.google.com"), desc="translate"),
        Key([], "y", lazy.spawn(
            "microsoft-edge-dev https://youtube.com"), desc="youtube"),
        Key([], "w", lazy.spawn(
            "microsoft-edge-dev https://wallhaven.com"), desc="wallhaven"),
        Key([], "r", lazy.spawn(
            "microsoft-edge-dev --inprivate https://doc.rust-lang.org/book/"), desc="the rust book"),
    ]),
]

group_names = [("乾", {'layout': 'columns'}),
               ("坤", {'layout': 'columns'}),
               ("震", {'layout': 'columns'}),
               ("巽", {'layout': 'columns'}),
               ("坎", {'layout': 'columns'}),
               ("离", {'layout': 'columns'}),
               ("艮", {'layout': 'columns'}),
               ("兑", {'layout': 'columns'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    # Switch to another group
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(
        name, switch_group=True)))  # Send current window to another group

layout_theme = {"border_width": 0,
                "margin": 0,
                "border_focus": "bdc4c9",
                "border_normal": "1D2330"
                }
layouts = [
    layout.Columns(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Stack(num_stacks=2),
    layout.Bsp(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.Tile(**layout_theme),
    # layout.TreeTab(**layout_theme),
    layout.VerticalTile(**layout_theme),
    layout.Zoomy(**layout_theme),
    layout.Floating(**layout_theme),
]

colors = [["#282c34", "#282c34"],  # panel background
          ["#3d3f4b", "#434758"],  # background for current screen tab
          ["#ffffff", "#ffffff"],  # font color for group names
          ["#ff5555", "#ff5555"],  # border line color for current tab
          # border line color for 'other tabs' and color for 'odd widgets'
          ["#74438f", "#74438f"],
          ["#4f76c7", "#4f76c7"],  # color for the 'even widgets'
          ["#e1acff", "#e1acff"],  # window name
          ["#ecbbfb", "#ecbbfb"]]  # backbround for inactive screens

widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            widgets=[
                widget.Sep(
                    linewidth=0,
                    padding=10,
                    foreground=colors[2],
                    background=colors[0]
                ),
                widget.TextBox(
                    text="",
                    background=colors[0],
                    padding=0,
                    fontsize=18
                ),
                widget.Sep(
                    linewidth=0,
                    padding=10,
                    foreground=colors[2],
                    background=colors[0]
                ),
                widget.GroupBox(
                    font="Roboto",
                    fontsize=11,
                    margin_y=3,
                    margin_x=0,
                    padding_y=5,
                    padding_x=3,
                    borderwidth=3,
                    active=colors[2],
                    inactive=colors[2],
                    rounded=False,
                    highlight_color=colors[1],
                    highlight_method="block",
                    this_current_screen_border=colors[3],
                    # this_current_screen_border = ["#080807", "#080807"],
                    this_screen_border=colors[1],
                    other_screen_border=colors[2],
                    foreground=colors[2],
                    background=colors[0]
                ),
                widget.Sep(
                    linewidth=0,
                    padding=5,
                    foreground=colors[2],
                    background=colors[0]
                ),
                widget.CurrentLayoutIcon(
                    foreground=colors[2],
                    background=colors[0],
                    padding=5,
                    scale=0.8
                ),
                widget.Sep(
                    linewidth=0,
                    padding=20,
                    foreground=colors[2],
                    background=colors[0]
                ),
                widget.WindowName(
                    font="wqy-microhei",
                    foreground=colors[2],
                    background=colors[0],
                    padding=0
                ),
                # widget.Chord(
                #     # chords_colors = {
                #     #     ("#000000", "#ffffff"),
                #     # },
                #     fmt = '{}',
                #     markup = False,
                #     max_chars = 20,
                # ),
                widget.Prompt(
                    prompt="Run:  ",
                    font="Roboto Bold Italic",
                    padding=10,
                    foreground=colors[2],
                    background=colors[1]
                ),
                widget.TextBox(
                    text="",
                    background=colors[0],
                    padding=10,
                    fontsize=16,
                ),
                widget.CheckUpdates(
                    update_interval=1800,
                    display_format="Patches: {updates} ⟳ ",
                    foreground=colors[2],
                    execute=terminal + ' -e sudo pacman -Syu',
                    no_update_string='Latest',
                    background=colors[0]
                ),
                widget.TextBox(
                    text="|",
                    background=colors[0],
                    padding=2,
                    fontsize=20
                ),
                widget.CPU(
                    format=": {freq_current}GHz {load_percent}%",
                    foreground=colors[2],
                    background=colors[0],
                ),
                widget.TextBox(
                    text="|",
                    background=colors[0],
                    padding=2,
                    fontsize=20,
                ),
                widget.Memory(
                    format=": {MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}%",
                    foreground=colors[2],
                    background=colors[0],
                    mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(
                        terminal + ' -e htop')},
                ),
                widget.TextBox(
                    text="|",
                    background=colors[0],
                    padding=2,
                    fontsize=20,
                ),
                widget.DF(
                    fmt="{}",
                    format=": {uf}/{s} {r:.0f}%",
                    visible_on_warn=False,
                    foreground=colors[2],
                    background=colors[0],
                    warn_space=2,
                ),
                widget.TextBox(
                    text="|",
                    background=colors[0],
                    padding=2,
                    fontsize=20
                ),
                widget.Clock(
                    fmt=": {}",
                    foreground=colors[2],
                    background=colors[0],
                    format="%Y-%m-%d %a %I:%M %p"
                ),
                widget.TextBox(
                    text="|",
                    background=colors[0],
                    padding=2,
                    fontsize=20
                ),
                widget.TextBox(
                    text=" ",
                    background=colors[0],
                    padding=0,
                    fontsize=16,
                    mouse_callbacks={
                        'Button1': lambda: qtile.cmd_spawn('shutdown now')},
                ),
            ],
            background='#4a4c4b',
            size=20,
            opacity=0.7
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_width=1, border_focus="747b82", border_normal="404447")
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


# Customize by Aka

# Auto_start
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
